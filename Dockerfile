FROM python:3
WORKDIR /code
COPY ../ .
RUN pip install requirements.txt
RUN mkdir db 
EXPOSE 8000
CMD python manage.py runserver 0.0.0.0:8000

